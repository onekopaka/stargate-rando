require 'camping'
require 'json'

# Camping is a great small framework.
Camping.goes :StargateRando

# Data was moved out to stargaterando-data.rb to keep this file clean
require './stargaterando-data'

module StargateRando
	class Randomizer
		def self.generate_assistant_text
			series_selector = 1 + rand(17)
			case series_selector
			when 1..10
				chosen_series_id = 'SG-1'
			when 11..15
				chosen_series_id = 'SGA'
			else
				chosen_series_id = 'SGU'
			end

			@series = Data::SERIES_NAMES[chosen_series_id]
			num_seasons = Data::SEASONS_PER_SERIES[chosen_series_id]
			chosen_season = 1 + rand(num_seasons)

			# SG-1 has a couple of seasons of length 22 and one of length 21 so we need to handle that.
			case chosen_series_id
			when 'SG-1'
				case chosen_season
				when 1
					num_episodes = 21
				when 2..7
					num_episodes = 22
				else
					num_episodes = 20
				end
			else
				num_episodes = 20
			end

			chosen_episode = 1 + rand(num_episodes)

			@episode_and_season = "Season #{chosen_season} Episode #{chosen_episode}"
			@episode_name = Data::EPISODES["#{chosen_series_id}|#{chosen_season}x#{chosen_episode}"]

			return "You should watch: #{@series}: #{@episode_and_season}: #{@episode_name}"

		end
	end
end

# Controllers live here
module StargateRando::Controllers
	class Index < R '/'
		def get
			series_selector = 1 + rand(17)
			case series_selector
			when 1..10
				chosen_series_id = 'SG-1'
			when 11..15
				chosen_series_id = 'SGA'
			else
				chosen_series_id = 'SGU'
			end

			@series = Data::SERIES_NAMES[chosen_series_id]
			num_seasons = Data::SEASONS_PER_SERIES[chosen_series_id]
			chosen_season = 1 + rand(num_seasons)

			# SG-1 has a couple of seasons of length 22 and one of length 21 so we need to handle that.
			case chosen_series_id
			when 'SG-1'
				case chosen_season
				when 1
					num_episodes = 21
				when 2..7
					num_episodes = 22
				else
					num_episodes = 20
				end
			else
				num_episodes = 20
			end

			chosen_episode = 1 + rand(num_episodes)

			@episode_and_season = "Season #{chosen_season} Episode #{chosen_episode}"
			@episode_name = Data::EPISODES["#{chosen_series_id}|#{chosen_season}x#{chosen_episode}"]
			render :results
		end
	end

	class Alexa < R '/alexa'
		def get
			post()
		end

		def post
			@headers['Content-Type'] = "application/json"
			
			result_hash = {:version => "1.0", :response => {
				:outputSpeech => {
					:type => "PlainText",
					:text => StargateRando::Randomizer.generate_assistant_text
				},
				:shouldEndSession => true
				}
			}

			JSON.generate(result_hash)
		end
	end

	class GoogleActions < R '/google'
		def get
			post()
		end

		def post
			@headers['Content-Type'] = "application/json"

			result_hash = { :fulfillmentText => StargateRando::Randomizer.generate_assistant_text }

			JSON.generate(result_hash)
		end
	end
end

# This holds our views. Markaby is pretty swagtacular.
module StargateRando::Views
	def layout
		html do
			head do
				meta :charset => "UTF-8"
				title { "Stargate Episode Randomizer" }
				meta :name => "viewport", :content => "width=device-width, user-scalable=no"
				link :rel => "stylesheet", :href => "https://fonts.googleapis.com/css?family=Fira+Sans:400,700", :type => "text/css"
				link :rel => "stylesheet", :href => "main.css", :type => "text/css"
			end

			body { self << yield }
		end
	end

	def results
		h1 "You should watch: "
		h2.result! "#{@series}: #{@episode_and_season}: #{@episode_name}"
		button :onclick => "document.location.reload()", :id => "reload" do
			"Get a new episode"
		end

		p do
			span do
				"Perhaps you might want to check out "
			end
			a :href => "stargateorder.html" do
				"The Order of Watching Stargate"
			end
		end
	end

end
